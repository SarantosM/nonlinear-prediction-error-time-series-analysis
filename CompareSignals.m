clear all
clc
close all

[nrows, ncols] = size(x);

%Demeanetize data (mean = 0, var = 1)

for i = 1:ncols
    x(:,i) = m0v1(x(:,i));
end

%Strings 
Strings = {'Lorenz','Noisy Lorenz','EEG','Noisy Autoregressive','White Noise'};
Markers = {'+','o','*','x','v','d','^','s','>','<'};
Colors = {'b','k','g','m','r'};


%Plot Subplot of Time Series
figure(1) = figure('units','normalized','outerposition',[0 0 1 1]);
for i = 1:ncols
    subplot(ncols,1,i)   
    plot(0:nrows-1,x(:,i),'color',Colors{i})
    t(i) = title([Strings(i),' Time Series']);
    xlabel('time a.u.')
    ylabel('x')    
end
filepath = strcat(pwd,'\DelayPhaseSpace\Figures\')
filename = strcat(filepath,'Timeseries1.png');
saveas(figure(1),filename);


%Delay Phase Space
figure
tau = 2;
for i = 1:ncols
    subplot(1,ncols,i)
    plot3(x(2*tau+1:end,i),x(tau+1:end-tau,i),x(1:end-2*tau,i),'color',Colors{i})
    xlabel('x')
    ylabel('x-tau')
    zlabel('x-2tau')
    title([Strings{i},': tau = ',num2str(tau)])
    view(2)
end


%Distance Matrix
figure(2) = figure('units','normalized','outerposition',[0 0 1 1]);
tau = 2;
m = 6;
for i = 1:ncols
    subplot(ncols,ncols,i)
    [dist] = distmatrix(delayphasespace(x(:,i),tau,m),tau,m);
    imagesc(dist);
    colorbar
    title([Strings{i},': tau =',num2str(tau),' m = ',num2str(m)])
    view(2)
end

%Nonlinear Prediction Error
m = 6; k = 3; h = 2; W = 30;
tic;
for i = 1:ncols
    for tau = 1:30
        xerr(tau,i) = NLPE(x(:,i),m,k,h,W, tau);   
    end
end
disp(['Nonlinear prediction error for ',num2str(ncols),' signals of N = ',num2str(length(x))])
toc;

%%%%%%%%%%--------Plot Nonlinear Prediction Error-------%%%%%%%%%
figure
grid on
for i = 1:ncols
    hold on;
    plot(xerr(:,i),'color',Colors{i},'marker',Markers{i})
    title(['Nonlinear Prediction Error: m = ',num2str(m),' k = ',num2str(k)...
        ,' h = ',num2str(h),' W = ',num2str(W),' N = ',num2str(N)]);
    Legend{i} = strcat(Strings{i});
    xlabel('tau'); ylabel('NLPE');
end
legend(Legend)

%figure

handle = axes('Parent', figure(1));
figure(1) = figure('units','normalized','outerposition',[0 0 1 1]);
hold(handle, 'off');

for tau = 1:5; % N-1 %round(N/2)
%         plot3(x(2*tau+1:end),x(tau+1:end-tau),x(1:end-2*tau))
%         title(['Delay Reconstruction: tau = ',num2str(tau)]);
%         xlabel('x(t)'); ylabel('x(t-tau)'); zlabel('x(t-2tau)');
     
    for i = 1:ncols
         subplot(1,ncols,i)
         plot(x(1:end-tau,i),x(tau+1:end,i),'color',Colors{i})
         title([Strings{i}]);
    end
    
    %sgtitle(['Reconstructed Delay Phase Space: N = ',num2str(N),' tau = ',num2str(tau)])
     
     drawnow

     pause(0.1)
     %%FigH = figure('Position', get(0, 'Screensize'));
     % = getframe(FigH);
     %imwrite(F.cdata, 'Foos.png', 'png')
end



clear figure
handle = axes('Parent', figure(1));
figure(1) = figure('units','normalized','outerposition',[0 0 1 1]);
hold(handle, 'off');


for tau = 1:20; % N-1 %round(N/2)
%         plot3(x(2*tau+1:end),x(tau+1:end-tau),x(1:end-2*tau))
%         title(['Delay Reconstruction: tau = ',num2str(tau)]);
%         xlabel('x(t)'); ylabel('x(t-tau)'); zlabel('x(t-2tau)');
     
    for i = 1:ncols
         subplot(1,ncols,i)
         plot(x(1:end-tau,i),x(tau+1:end,i),'color',Colors{i})
         title([Strings{i},' tau = ',num2str(tau)]);
     end
     sgtitle(['Reconstructed Delay Phase Space: N = ',num2str(N)])
     drawnow
     
     %tic;
     filepath = strcat(pwd,'\DelayPhaseSpace\Figures\')
     filename = strcat(filepath,'RDPS',num2str(tau),'.png');
     %FigH = figure('Position', get(0, 'Screensize'));
     saveas(figure(1),filename);
     %toc;
     pause(0.1)
     %%FigH = figure('Position', get(0, 'Screensize'));
     % = getframe(FigH);
     %imwrite(F.cdata, 'Foos.png', 'png')
end
