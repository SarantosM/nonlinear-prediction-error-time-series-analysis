function [delays] = delayphasespace(x, tau,m)
x = x(:); %Ensure column vector
N = length(x);
hta = (m-1)*tau;

% Total length of non zero points on phase space
L = N - hta;

% Initialize the phase space Matrix
delays=zeros(L,m);

% Phase space reconstruction
for i=1:L
    delays(i,:)=x(i+(0:m-1)*tau)';
end

%Add zero padding at the start to keep same sample size
delays = fliplr([zeros(hta,m); delays]);
end