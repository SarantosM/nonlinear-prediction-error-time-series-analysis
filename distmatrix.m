function [dist] = distmatrix(emb,tau,m);

hta = (m-1)*tau;            %Define (m-1)tau as hta
N = length(emb);
dist = ones(N,N)*inf;       %Preallocate NxN size for distance matrix
for i = hta+1:N
    for j = i+tau:N  
        dist1 = 0;
        for k = 1:m
        %dist = (x(i-r*tau)-x(j-r*tau)).^2;
        dist1 = dist1 + (emb(i,k)-emb(j,k))^2;
        end
        dist(i,j) = dist1;dist(j,i) = dist1;       
    end
end    