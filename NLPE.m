function [xerr] = NLPE(x, m, k, h, W,tau);
%x   : time series for N < 5000
%m   : Embedding Dimension
%k   : number of neighbours to calculate distances
%h   : Prediction Horizon
%W   : Theiler Correction Length
%tau : Integer lag 

%Create delayed vectors
delays = delayphasespace(x,tau,m); 
N = length(delays);

%Preallocate NxN size for distance matrix with infinite values
dist = ones(N,N)*inf;
    %This speeds up the calculation but will produce an error for large
    %samples as Matlab is unable to store them

%Distance matrix but second loop starts from i+W instead of tau
for i = (m-1)*tau+1:N
    for j = i+W:N   
        dist1 = 0;      %Initialize variable as 0 for the sum in the inner loop
        for c = 1:m
        dist1 = dist1 + (delays(i,c)- delays(j,c))^2;
        end
        dist(i,j) = dist1;
        dist(j,i) = dist1;      %Use diagonal symmetry of distance matrix to speed up computation time  
    end
end    

dist= dist(1:length(dist) - h,1:length(dist) - h);      %Subtract the Prediction Horizon h
hta = (m-1)*tau; 
%Sort the distance matrix and obtain the corresponding indexes in ascending
%order

[~, index] = sort(dist);                                

%Preallocate Size of all Nonlinear Prediction Errors

epsiloni0 = zeros(length(dist) - hta - h,1);        
for i = hta+1:length(dist);
%Subtract the mean of sorted dist Matrix up to k nearest neighbours of the
%delay phase space from the unsorted delay phase space plus the prediction
%horizon h
    embdiff = delays(i+h,1) - mean(delays(index(1:k,i)+h,1));                                                                                                                        
    epsiloni0(i-hta) = embdiff^2;   %Square the embedded difference
end;
%Square root of the mean of all individual predictions
xerr = sqrt(mean(epsiloni0));       


%function [P] = NLPE(x,m,k,h,W)
%m is the dimension
%k is the number of nearest neighbors
%W is the theiler correction length
%h is the prediction horizon

%m = 6; k = 3; h = 2; W = 30;